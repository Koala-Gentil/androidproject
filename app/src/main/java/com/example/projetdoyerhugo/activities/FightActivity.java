package com.example.projetdoyerhugo.activities;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetdoyerhugo.R;

public class FightActivity extends AppCompatActivity {

    /**
     * Enumération pour le choix de l'action effectué par le joueur
     */
    public enum Action { FIGTH, ESCAPE }

    public final static String PLAYER_LIFE = "com.example.projetdoyerhugo.activities.FightActivity.PLAYER_LIFE";
    public final static String PLAYER_POWER = "com.example.projetdoyerhugo.activities.FightActivity.PLAYER_POWER";
    public final static String OPPONENT_POWER = "com.example.projetdoyerhugo.activities.FightActivity.OPPONENT_POWER";
    public final static String ACTION_RESULT = "com.example.projetdoyerhugo.activities.FightActivity.ACTION_RESULT";
    public final static String OPPONENT_IMAGE = "com.example.projetdoyerhugo.activities.FightActivity.OPPONENT_IMAGE";
    public final static String OPPONENT_NAME = "com.example.projetdoyerhugo.activities.FightActivity.OPPONENT_NAME";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fight);

        Intent intent = getIntent();

        int playerLife = intent.getIntExtra(PLAYER_LIFE, 0);
        int playerPower = intent.getIntExtra(PLAYER_POWER, 0);
        int opponentPower = intent.getIntExtra(OPPONENT_POWER, 0);
        int opponentImage = intent.getIntExtra(OPPONENT_IMAGE, R.drawable.explored_unkilled);
        String opponentName = intent.getStringExtra(OPPONENT_NAME);

        TextView playerLifeTextView = findViewById(R.id.life);
        playerLifeTextView.setText(String.valueOf(playerLife));

        TextView playerPowerTextView = findViewById(R.id.player_power);
        playerPowerTextView.setText(String.valueOf(playerPower));

        TextView opponentPowerTextView = findViewById(R.id.opponent_power);
        opponentPowerTextView.setText(String.valueOf(opponentPower));

        TextView opponentNameTextView = findViewById(R.id.opponent_name);
        opponentNameTextView.setText(opponentName);

        ImageView opponentImageView = findViewById(R.id.opponent_image);
        opponentImageView.setImageResource(opponentImage);
    }

    /**
     * Termine l'activité avec l'action choisi
     * @param action Action à retourner en résultat de l'activité
     */
    private void finish(Action action) {
        Intent resultIntent = new Intent();
        resultIntent.putExtra(ACTION_RESULT, action);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        finish(Action.ESCAPE);
        super.onBackPressed();
    }

    public void onEscapeClick(View view) {
        finish(Action.ESCAPE);
    }

    public void onFightClick(View view) {
        finish(Action.FIGTH);
    }
}
