package com.example.projetdoyerhugo.activities;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.Game;
import com.example.projetdoyerhugo.core.dungeon.MonsterRoom;
import com.example.projetdoyerhugo.core.dungeon.Room;
import com.example.projetdoyerhugo.core.dungeon.TreasureRoom;
import com.example.projetdoyerhugo.ui.RoomArrayAdapter;

public class MainActivity extends AppCompatActivity {

    private static final int FIGHT_REQUEST = 0;
    private static final int TREASURE_REQUEST = 1;

    private Game game;
    private GridView gridView;
    private TextView gameResultTextView;
    private TextView fightResultTextView;
    private TextView playerLifeTextView;
    private TextView playerPowerTextView;
    private TextView nbUnexploredRoomsTextView;
    private TextView levelTextView;
    private RoomArrayAdapter gridViewArrayAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.map);
        gameResultTextView = findViewById(R.id.game_result);
        fightResultTextView = findViewById(R.id.fight_result);
        playerLifeTextView = findViewById(R.id.life);
        playerPowerTextView = findViewById(R.id.player_power);
        nbUnexploredRoomsTextView = findViewById(R.id.nb_unexplored_rooms);
        levelTextView = findViewById(R.id.level);

        initializeGame();
    }

    private void initializeGame() {

        game = new Game();

        gridViewArrayAdapter = new RoomArrayAdapter(this, game.getDungeonLevel().getRooms());
        gridView.setNumColumns(game.getDungeonLevel().getSize());

        gridView.setAdapter(gridViewArrayAdapter);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Room room = gridViewArrayAdapter.getItem(position);
                if (!room.isEmpty() && !game.levelFinished()) {
                    goToRoom(room);
                }
            }
        });

        gameResultTextView.setText(getString(R.string.fight_result_label));
        fightResultTextView.setText("");

        displayInformation();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        switch (requestCode) {
            case FIGHT_REQUEST: {
                FightActivity.Action action = (FightActivity.Action) data.getSerializableExtra(FightActivity.ACTION_RESULT);
                switch (action) {
                    case FIGTH:
                        boolean playerWon = game.fightInRoom();
                        setFightResult(getString(playerWon ? R.string.fight_won : R.string.fight_lost));
                        break;
                    case ESCAPE:
                        game.escapeRoom();
                        setFightResult(getString(R.string.escape));
                        break;
                }
                break;
            }
            case TREASURE_REQUEST: {
                game.takeConsumableInRoom();
                break;
            }
        }

        if (game.levelFinished()) {
            game.goToNextLevel();
            gridViewArrayAdapter = new RoomArrayAdapter(this, game.getDungeonLevel().getRooms());
            gridView.setAdapter(gridViewArrayAdapter);
        }
        updateView();

        super.onActivityResult(requestCode, resultCode, data);
    }

    private void updateView() {
        gridView.invalidateViews();
        displayInformation();
    }

    private void goToRoom(Room room) {
        game.goToRoom(room);

        if (room instanceof MonsterRoom) {
            goToMonsterRoom((MonsterRoom) room);
        } else { // instanceof TreasureRoom
            goToTreasureRoom((TreasureRoom) room);
        }
    }

    private void goToMonsterRoom(MonsterRoom room) {
        Intent intent = new Intent(MainActivity.this, FightActivity.class);
        intent.putExtra(FightActivity.PLAYER_POWER, game.getPlayer().getPower());
        intent.putExtra(FightActivity.PLAYER_LIFE, game.getPlayer().getLife());
        intent.putExtra(FightActivity.OPPONENT_POWER, room.getMonster().getPower());
        intent.putExtra(FightActivity.OPPONENT_NAME, room.getMonster().getDisplayName());
        intent.putExtra(FightActivity.OPPONENT_IMAGE, room.getMonster().getImageRessource());
        startActivityForResult(intent, FIGHT_REQUEST);
    }

    private void goToTreasureRoom(TreasureRoom room) {
        game.goToRoom(room);
        Intent intent = new Intent(MainActivity.this, TreasureActivity.class);
        intent.putExtra(TreasureActivity.CONSUMABLE_IMAGE, room.getConsumable().getImageRessource());
        intent.putExtra(TreasureActivity.CONSUMABLE_DESCRIPTION, room.getConsumable().getDescription());
        startActivityForResult(intent, TREASURE_REQUEST);
    }

    private void displayInformation() {
        nbUnexploredRoomsTextView.setText(String.valueOf(game.getDungeonLevel().getNumberOfUnexploredRooms()));
        playerLifeTextView.setText(String.valueOf(game.getPlayer().getLife()));
        levelTextView.setText(String.valueOf(game.getLevel()));
        playerPowerTextView.setText(String.valueOf(game.getPlayer().getPower()));

        if (game.gameFinished()) {
            if (game.playerWon()) {
                gameResultTextView.setText(getString(R.string.game_won));
            } else {
                gameResultTextView.setText(getString(R.string.game_lost));
            }
        } else {
            gameResultTextView.setText(getString(R.string.fight_result_label));
        }
    }

    private void setFightResult(String fightResult) {
        TextView textView = findViewById(R.id.fight_result);
        textView.setText(fightResult);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.exit_menu_item:
                finish();
                return true;
            case R.id.restart_menu_item:
                initializeGame();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
