package com.example.projetdoyerhugo.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.projetdoyerhugo.R;


public class TreasureActivity extends AppCompatActivity {

    public final static String CONSUMABLE_IMAGE = "com.example.projetdoyerhugo.activities.TreasureActivity.CONSUMABLE_IMAGE";
    public final static String CONSUMABLE_DESCRIPTION = "com.example.projetdoyerhugo.activities.TreasureActivity.CONSUMABLE_DESCRIPTION";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treasure);

        Intent intent = getIntent();

        int consumableImage = intent.getIntExtra(CONSUMABLE_IMAGE, R.drawable.chest);
        String consumableDescription = intent.getStringExtra(CONSUMABLE_DESCRIPTION);

        TextView consumableDescriptionText = findViewById(R.id.consumable_description);
        consumableDescriptionText.setText(consumableDescription);

        ImageView consumableImageView = findViewById(R.id.consumable_image);
        consumableImageView.setImageResource(consumableImage);
    }

    public void onOkClick(View view) {
        setResult(RESULT_OK);
        finish();
    }

}
