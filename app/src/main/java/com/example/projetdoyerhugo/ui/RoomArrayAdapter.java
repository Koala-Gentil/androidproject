package com.example.projetdoyerhugo.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.dungeon.Room;

import java.util.List;

public class RoomArrayAdapter extends ArrayAdapter<Room> {
    private Context context;
    private List<Room> rooms;

    public RoomArrayAdapter(@NonNull Context context, @NonNull List<Room> rooms) {
        super(context, 0, rooms);
        this.context = context;
        this.rooms = rooms;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View listItem = convertView;
        if(listItem == null)
            listItem = LayoutInflater.from(context).inflate(R.layout.room, parent,false);

        Room currentRoom = rooms.get(position);

        ImageView image = listItem.findViewById(R.id.room_image);
        image.setImageResource(currentRoom.getImageRessource());

        return listItem;
    }
}
