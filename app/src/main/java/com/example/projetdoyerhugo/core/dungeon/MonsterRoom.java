package com.example.projetdoyerhugo.core.dungeon;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.entities.monsters.Monster;

public class MonsterRoom extends Room {

    private Monster monster;

    public MonsterRoom(Monster monster) {
        this.monster = monster;
    }

    public void removeMonster() {
        this.monster = null;
    }

    public Monster getMonster() {
        return monster;
    }

    @Override
    public boolean isEmpty() {
        return monster == null;
    }

    @Override
    public int getImageRessource() {
        if (isVisited()) {
            if (isEmpty()) {
                return R.drawable.killed;
            } else {
                return R.drawable.explored_unkilled;
            }
        } else {
            return R.drawable.unexplored;
        }
    }
}
