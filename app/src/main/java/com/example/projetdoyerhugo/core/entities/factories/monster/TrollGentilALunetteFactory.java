package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.TrollAGrosNez;
import com.example.projetdoyerhugo.core.entities.monsters.TrollGentilALunette;

public class TrollGentilALunetteFactory implements IMonsterFactory {
    public TrollGentilALunette get(int level) {
        return new TrollGentilALunette(level);
    }
}
