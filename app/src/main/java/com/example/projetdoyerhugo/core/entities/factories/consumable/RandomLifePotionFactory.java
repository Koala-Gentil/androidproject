package com.example.projetdoyerhugo.core.entities.factories.consumable;

import com.example.projetdoyerhugo.core.entities.consumables.LifePotion;

import java.util.Random;

public class RandomLifePotionFactory implements IRandomConsumableFactory {

    public RandomLifePotionFactory() {

    }

    private static final int MAX_POWER = 3;
    private static final int MIN_POWER = 1;

    public LifePotion get() {
        Random random = new Random();
        return new LifePotion(random.nextInt(MAX_POWER - MIN_POWER + 1) + MIN_POWER);
    }
}
