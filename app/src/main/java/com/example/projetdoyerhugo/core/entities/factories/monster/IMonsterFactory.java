package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.Monster;

public interface IMonsterFactory<T extends Monster> {
    T get(int level);
}
