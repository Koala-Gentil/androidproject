package com.example.projetdoyerhugo.core.entities.consumables;

import com.example.projetdoyerhugo.core.entities.Player;

public abstract class Consumable {

    private int imageRessource;

    public Consumable(int imageRessource) {
        this.imageRessource = imageRessource;
    }

    public abstract void accept(Player player);

    public int getImageRessource() {
        return imageRessource;
    }

    public abstract String getDescription();
}
