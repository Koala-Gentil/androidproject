package com.example.projetdoyerhugo.core;

import com.example.projetdoyerhugo.core.dungeon.DungeonLevel;
import com.example.projetdoyerhugo.core.dungeon.MonsterRoom;
import com.example.projetdoyerhugo.core.dungeon.Room;
import com.example.projetdoyerhugo.core.dungeon.TreasureRoom;
import com.example.projetdoyerhugo.core.entities.Entity;
import com.example.projetdoyerhugo.core.entities.Player;
import com.example.projetdoyerhugo.core.entities.consumables.Consumable;

public class Game {

    private Player player;
    private DungeonLevel dungeonLevel;
    private Room currentRoom;
    private int level;

    public Game() {
        level = 1;
        player = new Player();
        dungeonLevel = new DungeonLevel(4, 1);
    }

    /**
     * Déplace le joueur dans la salle
     * @param room
     */
    public void goToRoom(Room room) {
        room.setVisited(true);
        currentRoom = room;
    }

    public void goToNextLevel() {
        level = level +1;
        dungeonLevel = new DungeonLevel(4, level);
        player.setLife(10);
    }

    /**
     * Combat dans la salle courante
     * @return true si le joueur a gagné, false sinon
     */
    public boolean fightInRoom() {
        MonsterRoom monsterRoom = (MonsterRoom) currentRoom;
        Arena arena = new Arena();
        arena.setFirstEntity(player);
        arena.setSecondEntity(monsterRoom.getMonster());
        Entity winner = arena.compute();
        if (winner == player) {
            monsterRoom.removeMonster();
            player.setPower(player.getPower() + 10);
            currentRoom = null;
            return true;
        } else {
            player.setLife(player.getLife() - 3);
            currentRoom = null;
            return false;
        }
    }

    public void takeConsumableInRoom() {
        TreasureRoom treasureRoom = (TreasureRoom) currentRoom;
        Consumable consumable = treasureRoom.getConsumable();
        player.consume(consumable);
        treasureRoom.removeConsumable();
        currentRoom = null;
    }

    /**
     * Fuit de la salle courante
     */
    public void escapeRoom() {
        currentRoom = null;
        player.setLife(player.getLife() - 1);
    }

    public boolean levelFinished() {
        return dungeonLevel.allRoomsAreEmpty();
    }

    public boolean gameFinished() {
        return player.getLife() <= 0;
    }

    public boolean playerWon() {
        return levelFinished() && player.getLife() > 0;
    }

    public Player getPlayer() {
        return player;
    }

    public DungeonLevel getDungeonLevel() {
        return dungeonLevel;
    }

    public int getLevel() {
        return level;
    }
}
