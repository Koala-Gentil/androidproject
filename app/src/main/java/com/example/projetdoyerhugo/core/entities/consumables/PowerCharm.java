package com.example.projetdoyerhugo.core.entities.consumables;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.entities.Player;

public class PowerCharm extends Consumable {

    private int power;

    public PowerCharm(int power) {
        super(R.drawable.charme_de_puissance);
        this.power = power;
    }

    @Override
    public void accept(Player player) {
        player.setPower(player.getPower() + power);
    }

    @Override
    public String getDescription() {
        return "Charme de puissance (" + power + ")";
    }
}
