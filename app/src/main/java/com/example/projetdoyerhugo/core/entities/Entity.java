package com.example.projetdoyerhugo.core.entities;

public abstract class Entity {

    public Entity(int power, int imageRessource) {
        this.power = power;
        this.imageRessource = imageRessource;
    }

    private int power;

    private int imageRessource;

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public int getImageRessource() {
        return imageRessource;
    }
}
