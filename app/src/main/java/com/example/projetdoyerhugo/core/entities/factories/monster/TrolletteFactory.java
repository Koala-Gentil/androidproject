package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.TrollAGrosNez;
import com.example.projetdoyerhugo.core.entities.monsters.Trollette;

public class TrolletteFactory implements IMonsterFactory {
    public Trollette get(int level) {
        return new Trollette(level);
    }
}
