package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.TrollAGrosNez;
import com.example.projetdoyerhugo.core.entities.monsters.TrollDeNoel;

public class TrollDeNoelFactory implements IMonsterFactory {
    public TrollDeNoel get(int level) {
        return new TrollDeNoel(level);
    }
}
