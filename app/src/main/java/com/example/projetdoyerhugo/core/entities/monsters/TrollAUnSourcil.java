package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class TrollAUnSourcil extends Monster {
    public TrollAUnSourcil(int level) {
        super(100, R.drawable.troll_a_un_sourcil, "Troll à un sourcil", level);
    }
}
