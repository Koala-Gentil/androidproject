package com.example.projetdoyerhugo.core.entities.consumables;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.entities.Player;

public class LifePotion extends Consumable {

    private int power;

    public LifePotion(int power) {
        super(R.drawable.potion);
        this.power = power;
    }

    @Override
    public void accept(Player player) {
        player.setLife(player.getLife() + power);
    }

    @Override
    public String getDescription() {
        return "Potion de vie (" + power + ")";
    }
}
