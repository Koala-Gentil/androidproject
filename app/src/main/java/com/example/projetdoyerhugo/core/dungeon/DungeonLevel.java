package com.example.projetdoyerhugo.core.dungeon;

import com.example.projetdoyerhugo.core.entities.factories.consumable.RandomLifePotionFactory;
import com.example.projetdoyerhugo.core.entities.factories.consumable.RandomPowerCharmFactory;
import com.example.projetdoyerhugo.core.entities.factories.monster.RandomMonsterFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class DungeonLevel {

    private List<Room> rooms;

    private int size;
    private int level;

    public DungeonLevel(int size, int level) {
        this.size = size;
        rooms = new ArrayList<>();

        RandomMonsterFactory randomMonsterFactory = new RandomMonsterFactory();
        RandomLifePotionFactory randomLifePotionFactory = new RandomLifePotionFactory();
        RandomPowerCharmFactory randomPowerCharmFactory = new RandomPowerCharmFactory();

        int nbRoom = size * size;
        for (int i = 0; i < nbRoom - 2; ++i) {
            rooms.add(new MonsterRoom(randomMonsterFactory.get(level)));
        }

        rooms.add(new TreasureRoom(randomLifePotionFactory.get()));
        rooms.add(new TreasureRoom(randomPowerCharmFactory.get()));

        Collections.shuffle(rooms);
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public int getSize() {
        return size;
    }

    /**
     * @return Nombre de pièces non explorées
     */
    public int getNumberOfUnexploredRooms() {
        int sum = 0;
        for(Room room: rooms) {
            if (!room.isVisited()) ++sum;
        }
        return sum;
    }

    public boolean allRoomsAreEmpty() {
        for(Room room: rooms) {
            if (!room.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
