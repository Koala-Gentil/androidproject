package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.TrollAGrosNez;
import com.example.projetdoyerhugo.core.entities.monsters.TrollQuiCracheDuFeu;

public class TrollQuiCracheDuFeuFactory implements IMonsterFactory {
    public TrollQuiCracheDuFeu get(int level) {
        return new TrollQuiCracheDuFeu(level);
    }
}
