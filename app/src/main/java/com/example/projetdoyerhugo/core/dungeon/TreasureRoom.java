package com.example.projetdoyerhugo.core.dungeon;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.entities.consumables.Consumable;

public class TreasureRoom extends Room {

    private Consumable consumable;

    public TreasureRoom(Consumable consumable) {
        this.consumable = consumable;
    }

    public Consumable getConsumable() {
        return consumable;
    }

    public void removeConsumable() {
        consumable = null;
    }

    @Override
    public boolean isEmpty() {
        return consumable == null;
    }

    @Override
    public int getImageRessource() {
        if (isVisited()) {
            return R.drawable.chest;
        } else {
            return R.drawable.unexplored;
        }
    }
}
