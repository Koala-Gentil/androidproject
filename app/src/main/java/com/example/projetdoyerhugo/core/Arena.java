package com.example.projetdoyerhugo.core;

import com.example.projetdoyerhugo.core.entities.Entity;

/**
 * Class pour simuler l'issue d'un combat
 */
public class Arena {
    private Entity firstEntity;
    private Entity secondEntity;

    public Arena() {

    }

    public Arena(Entity firstEntity, Entity secondEntity) {
        this.firstEntity = firstEntity;
        this.secondEntity = secondEntity;
    }

    /**
     * Simule le combat
     * @return l'entité qui a gagné le combat
     */
    public Entity compute()  {
        double effPower1 = firstEntity.getPower() * Math.random();
        double effPower2 = secondEntity.getPower() * Math.random();

        // retourne l'entité ayant la plus grande puissance effective
        return effPower1 > effPower2 ? firstEntity : secondEntity;
    }

    public Entity getFirstEntity() {
        return firstEntity;
    }

    public void setFirstEntity(Entity firstEntity) {
        this.firstEntity = firstEntity;
    }

    public Entity getSecondEntity() {
        return secondEntity;
    }

    public void setSecondEntity(Entity secondEntity) {
        this.secondEntity = secondEntity;
    }
}
