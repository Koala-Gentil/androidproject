package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class TrollQuiCracheDuFeu extends Monster {
    public TrollQuiCracheDuFeu(int level) {
        super(150, R.drawable.troll_qui_crache_du_feu, "Troll qui crache du feu", level);
    }
}
