package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.Monster;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Générateur aléatoire de monstre
 */
public class RandomMonsterFactory implements IMonsterFactory {

    static private List<IMonsterFactory> monsterFactories;
    static {
        monsterFactories = new ArrayList<>();
        monsterFactories.add(new TrolletteFactory());
        monsterFactories.add(new TrollGentilALunetteFactory());
        monsterFactories.add(new TrollAGrosNezFactory());
        monsterFactories.add(new TrollAUnSourcilFactory());
        monsterFactories.add(new TrollQuiCracheDuFeuFactory());
        monsterFactories.add(new TrollDeNoelFactory());
    }

    private Random random;

    public RandomMonsterFactory() {
        random = new Random();
    }

    /**
     * @return nouveau monstre aléatoirement choisi parmis tout les monstres possibles
     */
    public Monster get(int level) {
        int randomIndex = random.nextInt(monsterFactories.size());
        return monsterFactories.get(randomIndex).get(level);
    }
}
