package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class TrollDeNoel extends Monster {
    public TrollDeNoel(int level) {
        super(70, R.drawable.troll_de_noel, "Troll de noël", level);
    }
}
