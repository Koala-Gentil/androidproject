package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class Trollette extends Monster {
    public Trollette(int level) {
        super(40, R.drawable.trollette, "Trollette", level);
    }
}
