package com.example.projetdoyerhugo.core.dungeon;

public abstract class Room {

    private boolean isVisited;

    public Room() {
        isVisited = false;
    }

    public boolean isVisited() {
        return isVisited;
    }

    public void setVisited(boolean visited) {
        isVisited = visited;
    }

    public abstract boolean isEmpty();

    public abstract int getImageRessource();
}
