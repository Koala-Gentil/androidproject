package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.core.entities.Entity;

public abstract class Monster extends Entity {

    private String displayName;

    public Monster(int basePower, int imageRessource, String displayName, int level) {
        super(basePower * level, imageRessource);
        this.displayName = displayName;
    }

    /**
     * @return Nom affichable du monstre
     */
    public String getDisplayName() {
        return displayName;
    }
}
