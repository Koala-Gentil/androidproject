package com.example.projetdoyerhugo.core.entities.factories.monster;

import com.example.projetdoyerhugo.core.entities.monsters.TrollAGrosNez;

public class TrollAUnSourcilFactory implements IMonsterFactory {
    public TrollAGrosNez get(int level) {
        return new TrollAGrosNez(level);
    }
}
