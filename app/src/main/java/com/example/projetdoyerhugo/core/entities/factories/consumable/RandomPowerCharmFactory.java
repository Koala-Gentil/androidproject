package com.example.projetdoyerhugo.core.entities.factories.consumable;

import com.example.projetdoyerhugo.core.entities.consumables.PowerCharm;

import java.util.Random;

public class RandomPowerCharmFactory implements IRandomConsumableFactory {

    public RandomPowerCharmFactory() {

    }

    private static final int MAX_POWER = 10;
    private static final int MIN_POWER = 5;

    public PowerCharm get() {
        Random random = new Random();
        return new PowerCharm(random.nextInt(MAX_POWER - MIN_POWER + 1) + MIN_POWER);
    }
}
