package com.example.projetdoyerhugo.core.entities.factories.consumable;

import com.example.projetdoyerhugo.core.entities.consumables.Consumable;

public interface IRandomConsumableFactory {
    Consumable get();
}
