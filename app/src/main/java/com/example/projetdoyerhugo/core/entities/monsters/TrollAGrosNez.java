package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class TrollAGrosNez extends Monster {
    public TrollAGrosNez(int level) {
        super(60, R.drawable.troll_a_gros_nez, "Troll à gros nez", level);
    }
}
