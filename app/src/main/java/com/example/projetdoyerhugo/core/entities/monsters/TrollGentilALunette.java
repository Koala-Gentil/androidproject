package com.example.projetdoyerhugo.core.entities.monsters;

import com.example.projetdoyerhugo.R;

public class TrollGentilALunette extends Monster {
    public TrollGentilALunette(int level) {
        super(30, R.drawable.troll_gentil_a_lunette, "Troll gentil à lunette", level);
    }
}
