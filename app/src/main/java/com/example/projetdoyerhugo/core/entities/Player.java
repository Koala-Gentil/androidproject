package com.example.projetdoyerhugo.core.entities;

import com.example.projetdoyerhugo.R;
import com.example.projetdoyerhugo.core.entities.consumables.Consumable;

public class Player extends Entity {

    private static final int INITIAL_POWER = 100;
    private static final int INITIAL_LIFE = 10;

    private int life;

    public Player() {
        super(INITIAL_POWER, R.drawable.hero);
        this.life = INITIAL_LIFE;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = Math.max(0, life);
    }

    public void consume(Consumable consumable) {
        consumable.accept(this);
    }
}
