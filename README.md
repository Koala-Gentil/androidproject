Rapport projet android
==============================

- Pour la génération des entités, j'ai créer une classe abstraite `Entity` contenant un attribut puissance, le `Player` hérite de `Entity` et possède en plus des points de vie.

- Pour le système de combat, j'ai créer une classe `Arena` prenant en paramètre deux `Entity` et possède une méthode `compute` retournant l'`Entity` qui a gagné le combat.

- Pour l'affichage de la grille, j'ai utilisé un custom `ArrayAdapter` : `RoomArrayAdapter` pour que ma `gridView` puisse prendre en data directement des `Room`, la classe `RoomArrayAdapter` gère ensuite l'affichage.

- Pour l'activité de combat, `FightActivity`, elle reçoit en paramètre "extra" les données nécessaires à son affichage, elle retourne ensuite une `enum Action` permettant à la `MainActivity` de savoir quelle action a été réalisée. Je pensais au départ partagé l'instance de `Game` à travers un singleton, mais j'ai vu que cette méthode n'était pas "safe" (voir [ici](https://medium.com/@programmerr47/singletons-in-android-63ddf972a7e7)). Mais finalement cette activité ne sert que d'affichage et de choix d'actions, que cette activité n'est pas accès à l'instance du jeu augment le principe d'encapsulation.

- La structure du code est divisé en 2 parties principales:
    - `core` qui contient toute la logique du jeu
    - `activities` qui contient les activités et l'affichage du jeu

- L'état complet du jeu est contenu dans une instance de `Game`, `Game` possède un `DungeonLevel` contenant une liste de `Room`. Pour différencier une salle contenant un monstre ou un consommable, `Room` est une classe abstraite et possèdent deux implémentations `MonsterRoom` et `TreasureRoom`.

- Tous les monstres et consommables sont créers via une factory. La classe `RandomMonsterFactory` construit une liste avec les différentes implémentations de `IMonsterFactory`, la classe `RandomMonsterFactory` implémente donc aussi `IMonsterFactory` (Design Pattern: Composite).

Screens
-----------

![](./screens/screen1.PNG)
![](./screens/screen2.PNG)
![](./screens/screen3.PNG)
![](./screens/screen4.PNG)